<?php

namespace NewTitle\Test\TestCase\Controller\Api;

use Cake\TestSuite\IntegrationTestCase;

/**
 * Class MyPluginControllerTest
 * @package NewTitle\Test\TestCase\Controller\Api
 */
class MyPluginControllerTest extends IntegrationTestCase
{
    /**
     * @var array
     */
    public $fixtures = [
        'plugin.NewTitle.my_plugins',
    ];

    public function testGetById()
    {
        $this->post('new-title/api/my-plugin/get-by-id', ['id' => 1]);
        $this->assertNotEmpty((string)$this->_response->getBody());
    }

    public function testException()
    {
        $this->get('new-title/api/my-plugin/exception');
        $this->assertEquals('An Internal Server Error Occurred', (string)$this->_response->getBody());
    }
}
