<?php

namespace NewTitle\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * Class MyPluginsFixture
 * @package NewTitle\Test\Fixture
 */
class MyPluginsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id'           => [
            'type'          => 'integer',
            'length'        => 11,
            'unsigned'      => false,
            'null'          => false,
            'default'       => null,
            'comment'       => '',
            'autoIncrement' => true,
            'precision'     => null,
        ],
        'title'        => [
            'type'      => 'string',
            'length'    => 45,
            'null'      => false,
            'default'   => null,
            'collate'   => 'latin1_swedish_ci',
            'comment'   => '',
            'precision' => null,
            'fixed'     => null,
        ],
        'created'      => [
            'type'      => 'datetime',
            'length'    => null,
            'null'      => false,
            'default'   => null,
            'comment'   => '',
            'precision' => null,
        ],
        'modified'     => [
            'type'      => 'datetime',
            'length'    => null,
            'null'      => false,
            'default'   => null,
            'comment'   => '',
            'precision' => null,
        ],
        '_indexes'     => [
            'created'  => ['type' => 'index', 'columns' => ['created'], 'length' => []],
            'modified' => ['type' => 'index', 'columns' => ['modified'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options'     => [
            'engine'    => 'InnoDB',
            'collation' => 'latin1_swedish_ci',
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'title'        => 'Title 1',
            'created'         => '2018-01-01 00:00:00',
            'modified'        => '2018-01-01 00:00:00',
        ],
    ];
}
