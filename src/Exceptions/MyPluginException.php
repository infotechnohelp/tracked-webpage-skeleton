<?php

namespace NewTitle\Exceptions;

/**
 * Class MyPluginException
 * @package NewTitle\Exceptions
 */
class MyPluginException extends \Exception
{
    /**
     * PathCellValidationException constructor.
     *
     * @param string $message
     */
    public function __construct($message = "")
    {
        parent::__construct($message);
    }
}
