<?php

namespace NewTitle\Controller\Api;

use Cake\ORM\TableRegistry;
use NewTitle\Exceptions\MyPluginException;

/**
 * Class MyPluginController
 * @package NewTitle\Controller\Api
 */
class MyPluginController extends AppController
{
    /**
     * @return \Cake\Http\Response
     */
    public function getById()
    {
        return $this->response->withStringBody(
            json_encode(TableRegistry::get('MyPlugins')->get($this->request->getData('id')))
        );
    }

    /**
     * @throws \NewTitle\Exceptions\MyPluginException
     */
    public function exception()
    {
        throw new MyPluginException('Exception');
    }
}
