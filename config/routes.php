<?php

use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

Router::plugin(
    'NewTitle',
    ['path' => '/new-title'],
    function (RouteBuilder $routes) {
        $routes->prefix('api', function($routes) {
            /** @var $routes RouteBuilder */
            $routes->fallbacks(DashedRoute::class);
        });
        $routes->fallbacks(DashedRoute::class);
    }
);
